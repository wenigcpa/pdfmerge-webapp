#!/bin/sh

set -e

NAME="pdfwebapp"

docker stop $NAME || true
docker rm $NAME || true

docker run -d --name $NAME -p "80:8080" $(docker build . -q)
