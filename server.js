const express = require('express')
const app = express()
app.use(express.static('build'))

const server = app.listen(8080, (err) => {
  if (err) {
    console.log(err)
    console.log('Error starting server')
    process.exit(1)
  }
  console.log('Listening on port 8080')
})
