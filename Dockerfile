FROM alpine:latest
MAINTAINER Chance Hudson

RUN apk add --update nodejs-npm

COPY . /src
WORKDIR /src

RUN npm install && \
    npm run build:prod && \
    npm install express

CMD ["npm", "run", "server"]
