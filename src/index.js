import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import App from './App'
import Home from './Home'
import DocumentStore from './stores/document'
import axios from 'axios'

if (process.env.NODE_ENV === 'production') {
  axios.defaults.baseURL = 'https://backend.pdfmerge.ctheory.io'
} else {
  axios.defaults.baseURL = 'http://localhost:4000'
}
document.title = 'pdfmerge'

Vue.use(VueRouter)
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {},
  modules: {
    document: DocumentStore,
  },
  mutations: {},
  actions: {
    loadState: () => {},
  },
})

store.dispatch('loadState')

const router = new VueRouter({
  mode: 'hash',
  routes: [
    { path: '/', component: Home },
  ]
})

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
