import axios from 'axios'

export default {
  state: {
    documents: [],
    sets: [],
  },
  mutations: {},
  actions: {
    loadDocuments: async ({ state }, payload = {}) => {
      const { data } = await axios.get('/documents')
      state.documents = data
    },
    uploadDocument: async ({ state }, payload) => {
      const { data } = await axios.post('/documents', payload)
      state.documents = [...state.documents, ...data]
    },
    updateDocument: async ({ state, dispatch }, payload = {}) => {
      const { data } = await axios.put('/documents', payload)
      state.documents = state.documents.map((doc) => {
        if (doc._id === data._id) {
          return data
        }
        return doc
      })
    },
    deleteDocument: async ({ state, dispatch }, payload = {}) => {
      await axios.delete('/documents', {
        data: payload
      })
      await dispatch('loadDocuments')
    },
    loadSets: async ({ state }, payload) => {
      const { data } = await axios.get('/documentsets')
      state.sets = data
    },
    upsertSet: async ({ state, dispatch }, payload) => {
      await axios.put('/documentsets', payload)
      await dispatch('loadSets')
    },
    deleteSet: async ({ state, dispatch }, payload) => {
      await axios.delete('/documentsets', {
        data: payload
      })
      await dispatch('loadSets')
    },
    exportDocumentIds: async ({ state }, payload) => {
      let ids = payload
      if (Array.isArray(payload)) {
        ids = payload.join(',')
      }
      window.open(`${axios.defaults.baseURL}/documents/combine?ids=${ids}`)
    },
    exportSet: async ({ state, dispatch }, { _id }) => {
      const { documentIds } = state.sets.find((s) => s._id === _id)
      dispatch('exportDocumentIds', documentIds)
    }
  },
}
