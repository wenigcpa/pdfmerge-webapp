## pdfmerge webapp

Vue.js based web interface.

### Dev server

Runs on port 8080

`npm start`

### Production server

Run in the source directory.

`docker run -d --restart always --name pdfmerge_web -p 8080:8080 $(docker build . -q)`
